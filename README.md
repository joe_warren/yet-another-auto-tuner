# YAAT ▦ Yet Another Auto Tuner #

YAAT is an Auto-Tuner that uses Cepstral analysis for pitch detection, and a Short time Fourier Transform to pitch shift audio. 

### Disclaimer

This was written as a learning exercise, and is no where near good enough for professional use, unless the profession is Dalek impersonator.

### Licence 

Please consider this code available under a *do what you wilt* license. The code is supplied with no warranties, etc. etc.    

### Other Information 
The use of Cepstral analysis as a pitch detection method is optimised for human voice. Pitch detection may be suboptimal if other sound sources are used.  

Midi input (supplied over the ALSA audio framework) is used to specify the pitch to tune to. A maximum of 10 simultaneous notes are possible, allowing for a harmony to be synthesized. This can be changed by  modifying the POLYPHONY macro in the source code. 

ALSA is used as an audio framework because the ALSA libraries have good MIDI support, however ALSA has some limitations on modern systems.