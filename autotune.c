#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <alsa/asoundlib.h>

#define MIN(x,y) (x<y?x:y)

#define SWAP(type, i, j) {type t = i; i = j; j = t;}

#define POLYPHONY 10

void openMicrophone( snd_pcm_t** handle, char* name, unsigned int *rate,
       unsigned int buffer_size ){
  int err;
  snd_pcm_hw_params_t *hw_params;

  if ((err = snd_pcm_open (handle, name, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    fprintf (stderr, "cannot open audio device %s (%s)\n", 
      name,
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
    fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_any (*handle, hw_params)) < 0) {
    fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_access (
               *handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    fprintf (stderr, "cannot set access type (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_format (
               *handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
    fprintf (stderr, "cannot set sample format (%s)\n",
      snd_strerror (err));
    exit (1);
  }
	
  if ((err = snd_pcm_hw_params_set_rate_near(*handle, hw_params, rate, 0)) < 0){
    fprintf (stderr, "cannot set sample rate (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_channels (*handle, hw_params, 2)) <0){
    fprintf (stderr, "cannot set channel count (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_periods (*handle, hw_params, 2, 0)) <0){
    fprintf (stderr, "cannot set period count (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_period_size (
              *handle, hw_params, buffer_size * 2, 0)) <0){
    fprintf (stderr, "cannot set period size (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params (*handle, hw_params)) < 0) {
    fprintf (stderr, "cannot set parameters (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  snd_pcm_hw_params_free (hw_params);
  if ((err = snd_pcm_prepare (*handle)) < 0) {
    fprintf (stderr, "cannot prepare audio interface for use (%s)\n",
      snd_strerror (err));
    exit (1);
  }
}


void openSpeaker( snd_pcm_t** handle, char* name, unsigned int *rate, 
       unsigned int buffer_size ){
  int err;
  snd_pcm_hw_params_t *hw_params;
  if ((err = snd_pcm_open (handle, name, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
    fprintf (stderr, "cannot open audio device %s (%s)\n", 
      name,
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
    fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_any (*handle, hw_params)) < 0) {
    fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_access (
               *handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    fprintf (stderr, "cannot set access type (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_format (
               *handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
    fprintf (stderr, "cannot set sample format (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_rate_near(*handle, hw_params, rate, 0)) < 0){
    fprintf (stderr, "cannot set sample rate (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_channels(*handle, hw_params, 2)) < 0) {
    fprintf (stderr, "cannot set channel count (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_periods (*handle, hw_params, 2, 0)) <0){
    fprintf (stderr, "cannot set period count (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params_set_period_size (
              *handle, hw_params, buffer_size * 2, 0)) <0){
    fprintf (stderr, "cannot set period size (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  if ((err = snd_pcm_hw_params (*handle, hw_params)) < 0) {
    fprintf (stderr, "cannot set parameters (%s)\n",
      snd_strerror (err));
    exit (1);
  }
  snd_pcm_hw_params_free (hw_params);
  if ((err = snd_pcm_prepare (*handle)) < 0) {
    fprintf (stderr, "cannot prepare audio interface for use (%s)\n",
      snd_strerror (err));
    exit (1);
  }
}



static snd_seq_t* openMidiSequencer( char * name ){

  snd_seq_t *seq_handle;
  int portid;

  if (snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_INPUT, 0) < 0) {
    fprintf(stderr, "Error opening ALSA sequencer.\n");
    exit(1);
  }
  snd_seq_set_client_name(seq_handle, name );
  if ((portid = snd_seq_create_simple_port(seq_handle, name,
        SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
        SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }

  return seq_handle;
}

void createHammingWindow( float * data, int length ){
  int i;
  for( i = 0; i < length; i++ ){
    data[i] = -0.46 * cos( 2.*M_PI*(double)i/(double)length ) + 0.54;
  }
}


struct processBuffers{
  float* newData;
  float* oldData;
  float* workingData;
  float* shiftedData;
  float* acumulator;
  float* cepstrum;
  float* window;
  float* synFreq;
  float* synMag; 
  float* anaFreq;
  float* anaMag; 
  float* lastPhase;
};

struct processBuffers* createProcessBuffers( buffer_size ){
  struct processBuffers* b = malloc( sizeof( struct processBuffers ) );
  b->newData = malloc( 2 * sizeof(float) * buffer_size ); 
  b->oldData = malloc( 2 * sizeof(float) * buffer_size );
  b->workingData = malloc( 2 * sizeof(float) * buffer_size ); 
  b->shiftedData = malloc( 2 * sizeof(float) * buffer_size ); 
  b->acumulator = malloc( 4 * sizeof(float) * buffer_size ); 
  b->cepstrum = malloc( 2 * sizeof(float) * buffer_size ); 
  b->window = malloc( sizeof(float) * buffer_size ); 
  b->synFreq = malloc( 2 * sizeof(float) * buffer_size ); 
  b->synMag = malloc( 2 * sizeof(float) * buffer_size ); 
  b->anaFreq = malloc( 2 * sizeof(float) * buffer_size ); 
  b->anaMag = malloc( 2 * sizeof(float) * buffer_size ); 
  b->lastPhase = malloc( sizeof(float) * buffer_size ); 

  memset( b->oldData, 0 ,2 * sizeof(float) * buffer_size ); 
  memset( b->acumulator, 0 ,2 * sizeof(float) * buffer_size ); 
  createHammingWindow( b->window, buffer_size );
  return b;
}

void deleteProcessBuffers( struct processBuffers* b ){
  free(b->oldData);
  free(b->newData);
  free(b->workingData);
  free(b->shiftedData);
  free(b->acumulator);
  free(b->cepstrum);
  free(b->window);
  free(b->synFreq);
  free(b->synMag);
  free(b->anaFreq);
  free(b->anaMag);
  free(b->lastPhase);
  free(b);
}

void dechannel( short* src, float* dest, int size ){
  int i;
  for( i = 0; i< size; i++ ){
    dest[i*2] = ( src[i*2] + src[i*2+1] )/(float)SHRT_MAX;
    dest[i*2+1] = 0.0;
  }
}

void rechannel( float* src, short* dest, int size ){
  int i;
  for( i = 0; i< size; i++ ){
    dest[i*2] = src[i*2];
    dest[i*2+1] = src[i*2];
  }
}

void buildWorkingData( float* working, float* old, float* new, int length,
   int step, int oversampling ){
  int newSize = length * 2 * step / oversampling;
  int oldSize = length * 2 - newSize;
  memcpy( working, old + length*2 - oldSize, oldSize *sizeof(float) );
  memcpy( working + oldSize, new, newSize * sizeof(float) );
}
  
void addToAcumulator( float* acumulator, float* data, float length,
       int step, int oversampling ){
  int start = 2.0 * length * step / oversampling;
  int i;
  for( i = 0; i < 2* length; i++ ){
    acumulator[ start + i ] += data[i];
  } 
}

void advanceAcumulator(float* acumulator, int size ){
  memcpy( acumulator, acumulator + 2* size, size * 2 * sizeof(float) );
  memset( acumulator + 2*size, 0, 2*size*sizeof(float) );
}


void applyWindow( float* data, float* window, int size ){
  int i;
  for( i =0; i< size; i++ ){
    data[i*2] *= window[i];
  }
}


void fft(float *fftBuffer, long fftFrameSize, long sign){
  float wr, wi, arg, *p1, *p2, temp;
  float tr, ti, ur, ui, *p1r, *p1i, *p2r, *p2i;
  long i, bitm, j, le, le2, k;
  for (i = 2; i < 2*fftFrameSize-2; i += 2) {
    for (bitm = 2, j = 0; bitm < 2*fftFrameSize; bitm <<= 1) {
      if (i & bitm) j++;
      j <<= 1;
    }
    if (i < j) {
      p1 = fftBuffer+i; p2 = fftBuffer+j;
      temp = *p1; *(p1++) = *p2;
      *(p2++) = temp; temp = *p1;
      *p1 = *p2; *p2 = temp;
    }
  }
  for (k = 0, le = 2; k < (long)(log(fftFrameSize)/log(2.)+.5); k++) {
    le <<= 1;
    le2 = le>>1;
    ur = 1.0;
    ui = 0.0;
    arg = M_PI / (le2>>1);
    wr = cos(arg);
    wi = sign*sin(arg);
    for (j = 0; j < le2; j += 2) {
      p1r = fftBuffer+j; p1i = p1r+1;
      p2r = p1r+le2; p2i = p2r+1;
      for (i = j; i < 2*fftFrameSize; i += le) {
        tr = *p2r * ur - *p2i * ui;
        ti = *p2r * ui + *p2i * ur;
        *p2r = *p1r - tr; *p2i = *p1i - ti;
        *p1r += tr; *p1i += ti;
        p1r += le; p1i += le;
        p2r += le; p2i += le;
      }
      tr = ur*wr - ui*wi;
      ui = ur*wi + ui*wr;
      ur = tr;
    }
  }
}

void calculateCepstrum( float* fftData, float* cepstrum, int length ){
  int i;
  float square;
  for( i = 0; i< length; i++ ){ 
    square = fftData[i*2] * fftData[i*2] + fftData[i*2 + 1] * fftData[i*2 + 1];
    // dividing the log by 2 is faster than taking the root
    cepstrum[i*2] = log(square)/2.0;
    cepstrum[i*2 +1] = 0;  
  }
  fft( cepstrum, length, 1 );
}


float calculatePitch( float* cepstrum, int length, int rate ){
  float max = 0;
  float idx = 0;
  float sz;
  int i, low, high;
  low = (int) floor(rate * 0.0005 );
  high = (int) floor( rate *0.02 ); 
  for( i=low; i<MIN(length, high); i++ ){
    sz = cepstrum[i*2] * cepstrum[i*2] + cepstrum[i*2+1] * cepstrum[i*2+1];
    if( sz > max ){
      max = sz;
      idx = i;
    }
  }
  return ((float)rate)/((float)idx);
} 

/* Frequency shift based on the short time Fourier transform */
/* This is loosely based on code that is (c)1999-2009 Stephan M. Bernsee */
/* URL: http://www.dspdimension.com */
void shiftData( float* data, float* dest, float length, float pitchShift,
                int rate, int osamp,
                struct processBuffers* s ){
  int i;
  int qpd, index;
  int stepSize = 2 * length/osamp;
  float freqPerBin = 0.5*rate/(double)length;
  float expct = 4.0*M_PI*(double)stepSize/(double)length;
  float  mag, phase, tmp;

  /* Step 1: analysis */
  for( i = 0; i< length; i++){
    mag = 2* sqrt( data[2*i] * data[2*i] + data[2*i+1] * data[2*i+1] );
    phase = atan2( data[2*i+1], data[2*i] );
    tmp = phase - s->lastPhase[i];
    s->lastPhase[i] = phase;
    tmp -= (double)i*expct;

    /* map delta phase into +/- Pi interval */
    qpd = tmp/M_PI;
    if (qpd >= 0) {
       qpd += qpd&1;
    } else {
      qpd -= qpd&1;
    }
    tmp -= M_PI*(double)qpd;
    /* get deviation from bin frequency from the +/- Pi interval */
    tmp = osamp*tmp/(2.*M_PI);

    /* compute the k-th partials' true frequency */
    tmp = (double)i*freqPerBin + tmp*freqPerBin;

    s->anaMag[i] = mag;
    s->anaFreq[i] = tmp;
  }
  /* Step 2: processing */
  memset(s->synMag, 0, 2*length*sizeof(float));
  memset(s->synFreq, 0, 2*length*sizeof(float));
  for (i = 0; i <= length; i++) { 
    index = i*pitchShift;
    if (index <= length) { 
      s->synMag[index] += s->anaMag[i]; 
      s->synFreq[index] = s->anaFreq[i] * pitchShift; 
    }
  } 
  /* Step 3: synthesis */
  for( i = 0; i < length; i++ ){
    /* get magnitude and true frequency from synthesis arrays */
    mag = s->synMag[i];
    tmp = s->synFreq[i];

    /* subtract bin mid frequency */
    tmp -= (double)i*freqPerBin;

    /* get bin deviation from freq deviation */
    tmp /= freqPerBin;

    /* take osamp into account */
    tmp = 2.*M_PI*tmp/osamp;

    /* add the overlap phase advance back in */
    tmp += (double)i*expct;

    phase = tmp;
    /* get real and imag part and re-interleave */
    dest[2*i] = mag*cos(phase);
    dest[2*i+1] = mag*sin(phase);
  }
  /* zero negative frequencies */
  for( i = length+2; i < 2*length; i++ ){
    dest[i] = 0.0;
  }
}


float noteToFrequency(int n){
  return 440.0 * pow(2,(n-57.0)/12.0);
}

void processData( short *recordedData, int rate, int bufferSize,
                  int oversampling,
                  int* notes_list,
                  struct processBuffers *s){
  int i, j;
  float pitch;
  float adjustment;
  dechannel( recordedData, s->newData, bufferSize ); 
  for( i = 1; i <= oversampling; i++ ){

    buildWorkingData( s->workingData, s->oldData, s->newData, bufferSize,
      i, oversampling );

    applyWindow( s->workingData, s->window, bufferSize );

    fft( s->workingData, bufferSize, -1 );

    calculateCepstrum( s->workingData, s->cepstrum, bufferSize );

    pitch = calculatePitch( s->cepstrum, bufferSize, rate );
    for( j = 0; j < POLYPHONY; j++ ){
      if( notes_list[j] == 0 ){
        continue;
      }
      adjustment = noteToFrequency( notes_list[j] )/pitch;

      shiftData( s->workingData, s->shiftedData,
        bufferSize, adjustment, rate, oversampling,s );

      fft( s->shiftedData, bufferSize, 1 ); 

      applyWindow( s->shiftedData, s->window, bufferSize );

      addToAcumulator( s->acumulator, s->shiftedData, bufferSize, 
        i, oversampling );
    }
  }
  rechannel( s->acumulator, recordedData, bufferSize ); 
  advanceAcumulator( s->acumulator, bufferSize );
  SWAP( float*, s->newData, s->oldData );
} 

void note_on( int note, int* notes_list ){
  int i;
  for( i= 0; i< POLYPHONY; i++ ){
    if( notes_list[i] == 0){
      notes_list[i] = note;
      break;
    }
  }
}

void note_off( int note, int* notes_list ){
  int i;
  for( i= 0; i< POLYPHONY; i++ ){
    if( notes_list[i] == note){
      notes_list[i] = 0;
    }
  }
}

void midi_action( snd_seq_t* midi_handle, int* notes_list ){
  snd_seq_event_t *ev;
  do {
    snd_seq_event_input(midi_handle, &ev);
    switch (ev->type) {
      case SND_SEQ_EVENT_NOTEON:
        note_on( ev->data.note.note, notes_list );
      break;        
      case SND_SEQ_EVENT_NOTEOFF:   
        note_off( ev->data.note.note, notes_list );
      break;        
    }
    snd_seq_free_event(ev);
  } while (snd_seq_event_input_pending(midi_handle, 0) > 0);
}


int main( int argc, char** argv ){
  snd_pcm_t* mike_handle;
  snd_pcm_t* speaker_handle;
  snd_seq_t* midi_handle;
  struct pollfd *pfd_midi;
  int npfd_midi;
  int err, i;
  struct processBuffers *pb;
  unsigned int rate = 44100;
  unsigned int buffersize = 2048;
  int oversampling = 4;
  char*  pcmName = "hw:0";
  char c;
  short* buf;
  int* notes_list = malloc( POLYPHONY * sizeof(int) ); 

  while ((c = getopt (argc, argv, "r:b:o:h")) != -1)
    switch (c)
    {
      case 'r':
        rate = (int)strtol( optarg, NULL, 10 );
        if( rate == 0 ){
          fprintf(stderr, "Invalid rate value: %s\n", optarg );
          exit( -1 );
        }  
        break;
      case 'b':
        buffersize = (int)strtol( optarg, NULL, 10 );
        if( buffersize == 0 ){
          fprintf(stderr, "Invalid period size: %s\n", optarg );
          exit( -1 );
        }  
        break;
      case 'o':
        oversampling = (int)strtol( optarg, NULL, 10 );
        if( oversampling == 0 ){
          fprintf(stderr, "Invalid oversampling level: %s\n", optarg );
          exit( -1 );
        } 
        break;
       case 'h':
         printf("%s: Yet Another Auto-Tuner\n"
           "\tAn Auto-Tuning Program\n"
           "\tUses Cepstral analysis and the short time Fourier transform\n"
           "\tto shift recorded sound to match the pitch of MIDI notes\n"
           "\n"
           "\tArguments:\n"
           "\t\t-r [SAMPLE__RATE] : %d\n"
           "\t\t-b [BUFFER__SIZE] : %d\n"
           "\t\t-o [OVERSAMPLING] : %d\n"
           "\t\t-h :: Display this help  message\n"
           , argv[0], rate, buffersize, oversampling );
         exit( 0 );
         break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }


  buf = malloc( 2 * sizeof(short) * buffersize );
  memset( notes_list, 0, POLYPHONY * sizeof( int ) );

  openMicrophone( &mike_handle, pcmName, &rate, buffersize );
  openSpeaker( &speaker_handle, pcmName, &rate, buffersize );
  midi_handle = openMidiSequencer( argv[0] );
 
  npfd_midi = snd_seq_poll_descriptors_count(midi_handle, POLLIN);
  pfd_midi = (struct pollfd *)alloca(npfd_midi * sizeof(struct pollfd));
  snd_seq_poll_descriptors(midi_handle, pfd_midi, npfd_midi, POLLIN);

  pb = createProcessBuffers( buffersize );

  for (i = 0; i < 10000; ++i) {

    if (poll(pfd_midi, npfd_midi, 0) > 0) {
      midi_action( midi_handle, notes_list );
    }  

    if ((err = snd_pcm_readi (mike_handle, buf, buffersize)) != buffersize){
      fprintf (stderr, "read from audio interface failed (%s)\n",
        snd_strerror (err));
      break;
    }

    processData( buf, rate, buffersize, oversampling,  notes_list, pb );

    if ((err = snd_pcm_wait (speaker_handle, 1000)) < 0) {
      fprintf (stderr, "wait failed (%s)\n", strerror (errno));
      break;
    }

    if ((err = snd_pcm_writei (speaker_handle, buf, buffersize)) != buffersize){
      if( (err == snd_pcm_recover(speaker_handle, err, 0)) < 0 ){
        fprintf (stderr, "write to audio interface failed (%s)\n",
          snd_strerror (err));
        break;
      } else {
        if ((err = snd_pcm_writei (speaker_handle, buf, buffersize)) 
              != buffersize){
          if( (err == snd_pcm_recover(speaker_handle, err, 0)) < 0 ){
            fprintf (stderr, "write to audio interface failed (%s)\n",
              snd_strerror (err));
            break;
          }
        }
      }
    }
  }
  snd_pcm_close (mike_handle);
  snd_pcm_close (speaker_handle);
  free(buf);
  free(notes_list);
  deleteProcessBuffers(pb);
  return( 0 );
}
